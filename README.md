# Pringles

### Old Design

* Original
* Original (Portuguese Flag From The 2018 World Cup)
* Paprika
* Roast Chicken & Herbs (Limited Edition)
* Sour Cream & Onion
* Ham & Cheese
* Ketchup
* Seasalt & Herbs
* Hot & Spicy
* Jamón
* Red Chilli (Limited Edition)
* Pizza
* Salt & Vinegar
* Spicy BBQ Ribs (Limited Edition)
* Cheese & Onion
* Tortilla Chips - Original
* Tortilla Chips - El Salted Original
* Tortilla Chips - Nacho Cheese
* Rice Fusion - Indian Chicken Tikka Masala
* Pulled Pork Burger (Limited Edition)
* Emmental
* Rice Fusion - Malaysian Red Curry
* Roast Beef & Mustard
* Flame Medium - Kickin' Sour Cream
* Flame Spicy - Spicy BBQ
* Flame Extra Hot - Cheese & Chilli

### New Design

* Pizza
* Flame Extra Hot - Cheese & Chilli
* Jamón
* Flame Spicy - Spicy BBQ
* Original
* Passport Flavours - Italian Style Pepperoni Pizza (Limited Edition)
* Passport Flavours - New York Style Cheeseburger (Limited Edition)
* Flame Medium - Sweet Chilli
* Flame Spicy - Spicy Chorizo
* Passport Flavours - Mexican Style Chilli Taco (Limited Edition)
* Passport Flavours - Middle East Style Roasted Pepper & Hummus (Limited Edition)
* Hot & Spicy
* Flame Spicy - Mexican Chilli & Lime
* Hot - Mexican Chilli & Lime
* Passport Flavours - Spanish Style Patatas Bravas (Limited Edition)
* Passport Flavours - Greek Style Tzatziki (Limited Edition)
* Passport Flavours - Italian Style Focaccia (Limited Edition)
* Passport Flavours - French Style Steak Frites (Limited Edition)
* Mystery Flavour - Super Mario
